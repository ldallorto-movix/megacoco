package com.movixla.service.megapromo.coco;
import cl.movix.gmm.client.*;
import cl.movix.gmm.shared.Info;
import cl.movix.gmm.shared.ResultadoCobro;
import cl.movix.gmm.shared.ResultadoEnvio;
import cl.movix.gmm.shared.mms.MessageContent;

import com.movix.shared.LinkID;
import com.movix.shared.Operador;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.HashMap;
import java.util.List;

/**
 * Manejo de envios singleton a GMM
 * @author Marcelo vega
 * Date: 10-03-2010
 * Time: 05:48:36 PM
*/
public class GMMSingletonBO implements SuscriptorGMM {

    private static GMMSingletonBO instance = new GMMSingletonBO();
    private HashMap<String, Enviador> enviadores;
    private ClienteGMM clienteGMM_CL = null;
    private static final Logger logger = LoggerFactory.getLogger(GMMSingletonBO.class);
    /**
      * Constructor
    */

    private GMMSingletonBO() {
        clienteGMM_CL = new ClienteGMM("cl.gmm.movixla.com", "Cliente Broadcast Manager CL");
        //clienteGMM_CL = new ClienteGMM("10.3.3.114", "Cliente Broadcast Manager CL");
        enviadores = new HashMap<String, Enviador>();
    }
    
    public static GMMSingletonBO getInstance() {
        return instance;
    }


    /**
     * Obtiene el enviador para un servicioprecio en particular.
    */
    public Enviador getEnviador(String servicio, String precio, Operador operador) {
        String key = servicio + "/" + precio + "@" + operador.getPais().name();

        Enviador enviador = enviadores.get(key);
        if (enviador == null) {
            logger.info("conexionDeEnvioYConfirmacion para servicio:" + servicio);
            enviador = clienteGMM_CL.conexionDeEnvioYConfirmacion(servicio, precio, this);
            enviadores.put(key, enviador);
        }

        return enviador;
    }

    @Override
    public void recepcionMMS(String arg0, String arg1, String arg2, String arg3, List<MessageContent> arg4, Info arg5,
            String arg6, Operador arg7, LinkID arg8) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void recepcionSMS(String arg0, String arg1, String arg2, String arg3, Info arg4, String arg5, Operador arg6,
            LinkID arg7) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void resultadoCobroAsync(ResultadoCobro arg0, String arg1, String arg2, String arg3, String arg4, LinkID arg5) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void recepcionDeConfirmacion(String arg0, ResultadoEnvio arg1) {
        // TODO Auto-generated method stub
        
    }

}
