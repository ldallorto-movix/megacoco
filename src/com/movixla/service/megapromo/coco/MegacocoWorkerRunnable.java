package com.movixla.service.megapromo.coco;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class MegacocoWorkerRunnable implements Runnable {

    protected Socket clientSocket = null;
    protected String serverText = null;
    private static final Logger LOGGER = LoggerFactory.getLogger(MegacocoWorkerRunnable.class);

    public MegacocoWorkerRunnable(Socket clientSocket, String serverText) {
        this.clientSocket = clientSocket;
        this.serverText = serverText;
    }

    public void run() {

        BufferedReader reader = null;
        BufferedWriter writer = null;
        try {
            reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            writer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            String data;

            data = reader.readLine();
            // IResponse response = new Demonio().test("12345678901", "Asunto");
            String response = new Demonio().procesaEnvioSatPush(data);
            //response.getStatusCode() + "|" + response.getStatusText() + "|" + response.getDetails()
            writer.write(response);
            writer.newLine();
            writer.flush();

            close(reader);
            close(writer);
            close(clientSocket);
        }
        catch (Exception e) {
            LOGGER.error("", e);
            throw new RuntimeException(e.getMessage(), e);
        }
        finally {
            close(reader);
            close(writer);
            close(clientSocket);
        }

    }

    private void close(Socket socket) {
        try {
            if (socket != null) {
                socket.close();
            }
        }
        catch (IOException e) {
            LOGGER.error("", e);
        }
    }

    private void close(BufferedWriter writer) {
        try {
            if (writer != null) {
                writer.close();
            }
        }
        catch (IOException e) {
            LOGGER.error("", e);
        }
    }

    private void close(BufferedReader reader) {
        try {
            if (reader != null) {
                reader.close();
            }
        }
        catch (IOException e) {
            LOGGER.error("", e);
        }
    }
}