package com.movixla.service.megapromo.coco;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MegacocoServer {

    protected final int serverPort = 24000;
    protected ServerSocket serverSocket;
    protected boolean isStopped = false;
    protected Thread runningThread = null;

    static final Logger LOGGER = LoggerFactory.getLogger(MegacocoServer.class);

    public MegacocoServer() {
        super();
    }
    
    public static void main(String[] args) {
        new MegacocoServer().run();
    }

    public void run() {

        LOGGER.info("MegacocoServer init");
        synchronized (this) {
            this.runningThread = Thread.currentThread();
        }
        openServerSocket();
        while (!isStopped()) {
            Socket clientSocket = null;
            try {
                LOGGER.debug("running on port {}", this.serverPort);
                clientSocket = this.serverSocket.accept();
            }
            catch (IOException e) {
                if (isStopped()) {
                    LOGGER.error("", e);
                    return;
                }
                LOGGER.error("", e);
                throw new RuntimeException("Error accepting client connection", e);
            }
            new Thread(new MegacocoWorkerRunnable(clientSocket, "Multithreaded Server")).start();
        }
        LOGGER.debug("MegacocoServer Stopped");
    }

    private synchronized boolean isStopped() {
        return this.isStopped;
    }

    public synchronized void stop() {
        this.isStopped = true;
        try {
            this.serverSocket.close();
        }
        catch (IOException e) {
            LOGGER.error("", e);
            throw new RuntimeException("Error closing server", e);
        }
    }

    private void openServerSocket() {
        try {
            this.serverSocket = new ServerSocket(this.serverPort);
        }
        catch (IOException e) {
            LOGGER.error("", e);
            throw new RuntimeException("Cannot open port " + this.serverPort, e);
        }
    }

}