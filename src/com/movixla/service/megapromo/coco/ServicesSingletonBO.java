package com.movixla.service.megapromo.coco;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.movixla.service.its.common.SendablePro;


/**
 * Clase ServicesSingletonBO
 * Clase utilizada para manejar los envios al ITS con un esquema singleton.
 * <p/>
 *
 * @author ldallorto

 */
public class ServicesSingletonBO {
	
	private static ServicesSingletonBO SINGLETON = null;
    private Handler handler = null;
    private static final Logger logger = LoggerFactory.getLogger(ServicesSingletonBO.class);


    private ServicesSingletonBO(Handler handler) {
        this.handler = handler;
    }

    /**
      * Obtiene instancia singleton del objeto.
    */
    public static ServicesSingletonBO getInstance() {
        if (SINGLETON == null) {
            synchronized (ServicesSingletonBO.class) {
                if (SINGLETON == null) {
                    logger.info("Ini Handler");
                    Handler handler = new Handler();
                    SINGLETON = new ServicesSingletonBO(handler);
                }
            }
        }
        return SINGLETON;
    }

    /**
     * Realiza un envio de cualquier tipo al ITS.
    */
    public boolean realizarEnvioSatPush(SendablePro sendable) throws Throwable {

        if (sendable == null) {
            logger.error("Mensaje nulo ");
            return false;
        }

        handler.enviarSatPush(sendable);

        return true;
    }
    


}
