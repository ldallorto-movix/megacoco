package com.movixla.service.megapromo.coco.client;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import com.movix.shared.util.URLManager;

public class MegacocoClient {

    private String coconutHost;
    private int coconutPort;

    public MegacocoClient(String host, int port) {
        this.coconutHost = host;
        this.coconutPort = port;
    }

    public String send(URLManager message) throws UnknownHostException, IOException {

        Socket client = null;
        BufferedWriter writer = null;
        BufferedReader reader = null;
        String line = null;

        try {
            client = new Socket(this.coconutHost, this.coconutPort);
            writer = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
            reader = new BufferedReader(new InputStreamReader(client.getInputStream()));

            writer.write(message.toString());
            writer.newLine();
            writer.flush();

            line = reader.readLine();

        }
        finally {
            
            try {
                if (reader != null)
                    reader.close();
            }
            catch (Exception e) {
            }
            
            try {
                if (writer != null)
                    writer.close();

            }
            catch (Exception e) {
            }

            try {
                if (client != null)
                    client.close();
            }
            catch (Exception e) {
            }

        }

        return line;
    }

}

