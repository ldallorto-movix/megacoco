package com.movixla.service.megapromo.coco;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cl.movix.gmm.client.Enviador;
import com.movix.sat.SatLib;
import com.movix.shared.DWInfo2;
import com.movix.shared.LinkID;
import com.movix.shared.Operador;
import com.movix.shared.util.URLManager;
import com.movixla.service.its.common.SendablePro;
import com.movixla.shared.model.target.UserTarget;
import com.movixla.shared.model.tripper.SatPushPro;
import com.movixla.shared.model.tripper.TripperPack;
import com.movixla.shared.model.tripper.sat.Display;
import com.movixla.shared.model.tripper.sat.GetInput;
import com.movixla.shared.model.tripper.sat.LaunchBrowser;
import com.movixla.shared.model.tripper.sat.Macro;
import com.movixla.shared.model.tripper.sat.SelectItem;
import com.movixla.shared.model.via.LaSpVia;
import com.movixla.shared.sp.ServicePrice;

public class Demonio {

    private static final Logger LOGGER = LoggerFactory.getLogger(Demonio.class);

    static {
    }

    public String procesaEnvioSatPush(String lineas) {

        try {

            LOGGER.debug(lineas);

            URLManager url = new URLManager(lineas);

            String movil = url.getParameter("movil");
            String la = url.getParameter("LA");

            String smsFinal = url.getParameter("smsFinal");
            String servicio = url.getParameter("servicio");
            LinkID link = LinkID.createFromStaticSource(url.getParameter("link"));
            int megapromoid = Integer.parseInt(url.getParameter("megapromoid"));
            String mensaje = url.getParameter("mensaje"); // @DISU separacion del elemento, primer caracter el tipo
            int segmento = Integer.parseInt(url.getParameter("segmento"));
            List<Macro> macros = construirMacros(mensaje);

            if (macros != null && !macros.isEmpty()) {
                DWInfo2 dwInfo = new DWInfo2(DWInfo2.TipoProducto.TP_MEGAPROMO, megapromoid, 0, segmento);

                Operador operador = Operador.getOperadorPorIdBD(Integer.parseInt(url.getParameter("operador")));
                SendablePro sendable = new SendablePro(new SatPushPro(la, smsFinal, macros),
                        new LaSpVia(true, la, new ServicePrice(servicio, "0")), new UserTarget(operador, movil),
                        new TripperPack(link, dwInfo));

                if (operador == Operador.ENTEL) {
                    Enviador enviadorGMM = GMMSingletonBO.getInstance().getEnviador(servicio, "0", operador);
                    List<String> codes = SatLib.getCodes(sendable);
                    for (String code : codes) {
                        enviadorGMM.enviaRaw(movil, code, la, link, dwInfo, true);
                    }
                }
                else {
                    ServicesSingletonBO.getInstance().realizarEnvioSatPush(sendable);
                }
                LOGGER.info("sendable enviado " + sendable.getId() + " " + movil);
                return "0|ok";
            }
            else {
                return "1|noMacros";
            }
        }
        catch (Throwable e) {
            LOGGER.error("", e);
            return "-1|errorThrowable";
        }
    }

    /**
     * mensaje del tipo @DISU, separados los elementos por @ y primer caracter indica el tipo
     * tipo I y S dos variables separadas por #
     * 
     * @param mensaje
     * @return
     */
    static final String SEPARADOR_ELEMENTOS = "\\@\\$";
    static final String SEPARADOR_INTRAELEMENTOS = "\\#\\&";

    public static List<Macro> construirMacros(String mensaje) {

        String[] arr = mensaje.split(SEPARADOR_ELEMENTOS);
        long tiempoInicio = System.currentTimeMillis(), tiempoFinal;

        List<Macro> macros = new ArrayList<Macro>();
        LOGGER.info("partes " + arr.length + " " + arr + " del mensaje " + mensaje);
        for (String parte : arr) {

            LOGGER.info("procesando parte " + parte);
            try {
                if (parte.startsWith("D")) {
                    if (null == parte.substring(1) || "".equals(parte.substring(1).trim())) {
                        LOGGER.error("construirMacros.WARNING->Display no ingresado en :" + parte);
                    }
                    else {
                        LOGGER.info("adding Display " + parte.substring(1));
                        macros.add(new Display(parte.substring(1)));
                    }
                }
                else if (parte.startsWith("I")) {
                    if (null == parte.substring(1) || "".equals(parte.substring(1).trim())) {
                        LOGGER.error("construirMacros.WARNING->Input no ingresado en " + parte);
                    }
                    else {
                        String[] arr2 = parte.substring(1).split(SEPARADOR_INTRAELEMENTOS);
                        macros.add(new GetInput(arr2[0], Boolean.parseBoolean(arr2[1])));
                        LOGGER.info("adding getinput " + arr2[0] + "-" + Boolean.parseBoolean(arr2[1]));
                    }
                }
                else if (parte.startsWith("S")) {
                    if (null == parte.substring(1) || "".equals(parte.substring(1).trim())) {
                        LOGGER.error("construirMacros.WARNING->Select no ingresado en " + parte);
                    }
                    else {
                        String[] arr2 = parte.substring(1).split(SEPARADOR_INTRAELEMENTOS);
                        macros.add(new SelectItem(arr2[0], arr2[1].split(",")));
                        LOGGER.info("adding select " + arr2[0] + "-" + arr2[1].split(","));
                    }
                }
                else if (parte.startsWith("U")) {
                    if (null == parte.substring(1) || "".equals(parte.substring(1).trim())) {
                        LOGGER.error("construirMacros.WARNING->URL no ingresado en " + parte);
                    }
                    else {
                        macros.add(new LaunchBrowser(parte.substring(1)));
                        LOGGER.info("adding url " + parte.substring(1));
                    }
                }
            }
            catch (Exception exc) {
                LOGGER.error("construirSat.Excepcion->:" + exc.getMessage() + " " + parte);
            }
        }
        tiempoFinal = System.currentTimeMillis();
        LOGGER.info("construirMacros->Fin. {mensaje,elementos}: {" + mensaje + "," + macros.size() + "} Tiempo:"
                + (tiempoFinal - tiempoInicio) + " ms");

        if (macros.isEmpty()) {
            LOGGER.error("construirMcros.Error->No hay elementos macros validos para el {mensajeId}: " + mensaje + "}");
            return null;
        }
        return macros;
    }

}
