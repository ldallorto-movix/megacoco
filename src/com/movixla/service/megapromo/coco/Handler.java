package com.movixla.service.megapromo.coco;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.movix.shared.DWInfo2;
import com.movix.shared.DWInfo2.TipoProducto;
import com.movix.shared.LinkID;
import com.movix.shared.Operador;
import com.movixla.service.its.client.OutcomeListener;
import com.movixla.service.its.client.SendingClient;
import com.movixla.service.its.common.Outcome;
import com.movixla.service.its.common.Sendable;
import com.movixla.service.its.common.SendablePro;
import com.movixla.service.user.common.User;
import com.movixla.shared.model.target.UserTarget;
import com.movixla.shared.model.tripper.SatPushPro;
import com.movixla.shared.model.tripper.TripperPack;
import com.movixla.shared.model.tripper.sat.Macro;
import com.movixla.shared.model.via.LaSpVia;
import com.movixla.shared.sp.ServicePrice;

/**
 * Clase Handler
 * Clase utilizada para manejar los envios al ITS.
 * <p/>
 *
 * @author ldallorto
 */
public class Handler implements OutcomeListener {

    private static SendingClient sendingClient = null;

    public static final String SEPARADORLINEA = "\\|";

    /**
     * Constructor.
     */
    public Handler() {

        sendingClient = new SendingClient("Megacoco");
        sendingClient.setAskForOutcomes(false);
    }

    /**
     * Realiza un envio al ITS.
     */
    public void enviarSatPush(SendablePro sendable) throws Exception {

       sendingClient.send(sendable);

    }

    @Override
    public void outcomeReceived(Outcome arg0) {
        // TODO Auto-generated method stub
        
    }

}
